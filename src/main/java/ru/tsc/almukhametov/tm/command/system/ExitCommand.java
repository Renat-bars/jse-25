package ru.tsc.almukhametov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.almukhametov.tm.command.AbstractCommand;
import ru.tsc.almukhametov.tm.constant.SystemDescriptionConst;
import ru.tsc.almukhametov.tm.constant.TerminalConst;

public final class ExitCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return TerminalConst.EXIT;
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return SystemDescriptionConst.EXIT;
    }


    @Override
    public void execute() {
        System.exit(0);
    }

}
